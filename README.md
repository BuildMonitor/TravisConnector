# Travis CI Connector for buildmonitor.io

The official buildmonitor.io connector for Travis CI.

## How to use

1. Open [run.buildmonitor.io](https://run.buildmonitor.io/) in your browser.
2. Open settings by clicking `Settings` in the top left corner or press `C` on your keyboard.<br />![open settings](images/open-settings.png)
3. Click `Add Connector`.<br />![add connector button](images/add-connector-button.png)
4. Click the `Travis CI` button.<br />![travis ci button](images/travis-ci-button.png)
5. Specify your username and the API token. You can get the API token from <https://travis-ci.org/account/preferences>.<br />![configure travis ci](images/configure-travis-ci-connector.png)
5. Press `Save`.<br />![save configuration](images/save.png)
